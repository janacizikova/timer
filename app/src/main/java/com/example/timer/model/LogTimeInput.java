package com.example.timer.model;

import com.orm.SugarRecord;

public class LogTimeInput extends SugarRecord {
    Integer hour;
    Integer minute;
    Integer second;

    public LogTimeInput() {
    }

    public LogTimeInput(Integer hour, Integer minute, Integer second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }
}
