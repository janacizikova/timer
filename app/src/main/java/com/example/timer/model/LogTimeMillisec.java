package com.example.timer.model;

import com.orm.SugarRecord;

public class LogTimeMillisec extends SugarRecord {

    private Long millisec;

    public LogTimeMillisec() {
    }

    public LogTimeMillisec(Long milisec) {
        this.millisec = milisec;
    }

    public Long getMillisec() {
        return millisec;
    }

    public void setMillisec(Long milisec) {
        this.millisec = milisec;
    }
}
