package com.example.timer.model;

public enum TimeType {
    HOUR,
    MINUTE,
    SECOND;
}
