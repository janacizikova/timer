package com.example.timer.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.timer.R;
import com.example.timer.model.LogTimeMillisec;

/**
 * Created by Jana on 02/11/2017.
 */

public class LogAdapter extends BaseAdapter {

    private Context mainContext;
    private ServiceDbMillisec serviceDatabase;



    //konstruktor
    public LogAdapter(Context context) {
        this.mainContext = context;
        this.serviceDatabase = new ServiceDbTimeMillisecImpl() {
        };
    }

    //vrati pocet prvku seznamu
    @Override
    public int getCount() {
        return serviceDatabase.listAll().size();
    }

    //vrati jednu poznamku ze seznamu, ktery obsahuje obsah sugar db
    @Override
    public LogTimeMillisec getItem(int position) {
        //v position dostávám čísla od 0 podle prvků v listView, kdežto v DB mám číslování od nula, proto přičítám 1
        //když budu záznamy mazat, tak budu mít s tímhle problém
        return serviceDatabase.getLogTimeInputById(position + 1);
    }


    //vrati id logu ze seznamu obsahu sugar DB podle id
    @Override
    public long getItemId(int position) {
        return serviceDatabase.getLogTimeInputById(position + 1).getId();
    }


    //vrati nam jeden radek z ListView (prvek layoutu) naplneny hodnotami
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        System.out.println("position jest" + position);

        //najdu si layout, kde mam vizual pro jeden radek listView
        View result = LayoutInflater.from(mainContext).inflate(R.layout.activity_list_log_detail, viewGroup, false); //toto je result, co nam to vrati



        //z objektu poznamky ze seznamu si vyplnim udaje do jednoho prvku listview

        TextView logText = result.findViewById(R.id.log);
        LogTimeMillisec logTimeMillisec = getItem(position);

        TimeFormatter timeFormatter = new TimeFormatter();
        String s = timeFormatter.formatMillisecondToHourMinuteSecond(logTimeMillisec.getMillisec(), "Čas hod: %02d min: %02d sec: %02d");

        logText.setText(s);


        return result;
    }


}


