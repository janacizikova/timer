package com.example.timer.presenter;


import com.example.timer.model.LogTimeMillisec;

import java.util.List;

public interface ServiceDbMillisec {

    public void save(LogTimeMillisec newLog);

    public List<LogTimeMillisec> listAll();

    public LogTimeMillisec getLogTimeInputById(int id);

}
