package com.example.timer.presenter;

import com.example.timer.model.LogTimeMillisec;

import java.util.List;

public class ServiceDbTimeMillisecImpl implements ServiceDbMillisec {


    @Override
    public void save(LogTimeMillisec newLog) {
        newLog.save();

    }

    @Override
    public List<LogTimeMillisec> listAll() {
        return LogTimeMillisec.listAll(LogTimeMillisec.class);
    }

    @Override
    public LogTimeMillisec getLogTimeInputById(int id) {
        return LogTimeMillisec.findById(LogTimeMillisec.class, id);
    }
}
