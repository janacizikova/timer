package com.example.timer.presenter;

import android.content.Context;
import android.content.res.Resources;
import android.media.MediaPlayer;


import com.example.timer.R;


public class SoundPlayer {

    public static MediaPlayer mediaPlayer;

    public void playSound(Context context, String soundTitle) {

        Resources res = context.getResources();
        int soundId = res.getIdentifier(soundTitle, "raw", context.getPackageName());


//        mediaPlayer = MediaPlayer.create(context, R.raw.tijuana_taxi_herb_alpert);
        mediaPlayer = MediaPlayer.create(context, soundId);
        mediaPlayer.start();
    }

    public static void stopSound() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }
}
