package com.example.timer.presenter;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TimeFormatter {


//    př. String "Zbývá hod: %02d min: %02d sec: %02d"
//    https://stackoverflow.com/questions/41588943/how-to-display-minutes-and-seconds-in-countdowntimer

    public String formatMillisecondToHourMinuteSecond(Long timeInMilisec, String textToFormat) {
        String text = String.format(Locale.getDefault(), textToFormat,
                TimeUnit.MILLISECONDS.toHours(timeInMilisec) % 60,
                TimeUnit.MILLISECONDS.toMinutes(timeInMilisec) % 60,
                TimeUnit.MILLISECONDS.toSeconds(timeInMilisec) % 60);
        return text;
    }
}
