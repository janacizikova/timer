package com.example.timer.presenter;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.timer.model.LogTimeMillisec;
import com.example.timer.model.TimeType;

public class Timer {

    // service třídy (závislosti)
    private CountDownTimer countDownTimer;
    private SoundPlayer soundPlayer;
    private ServiceDbTimeMillisecImpl serviceDbTimeMillisec;
    private TimeFormatter timeFormatter;

    // pomocné proměnné
    private Long timeRemaning;
    private Long timeForLog;


    public Timer() {
        serviceDbTimeMillisec = new ServiceDbTimeMillisecImpl();
        timeFormatter = new TimeFormatter();
        soundPlayer = new SoundPlayer();
    }

    /**
     * spustí odpočítávání, uloží záznam do DB
     *
     * @param time délka času (v milisekundách) zadaná uživatelem
     * @param timeText nastavení textu do layoutu aplikace
     * @param startPauseBtn
     */

    public void startTimer(final long time, final TextView timeText, final TextView timeTotalFromDB, final ToggleButton startPauseBtn, final Context context) {
        timeForLog = time;
        countDownTimer = new CountDownTimer(time, 1000) {

            /*
            v daném intervalu (1 sekunda) provádí akce: vypisuje zbývající čas v layoutu
             */
            @Override
            public void onTick(long millisecondUntilFinished) {
                timeRemaning = millisecondUntilFinished;
                String textMillisecondUntilFinished = timeFormatter.formatMillisecondToHourMinuteSecond(millisecondUntilFinished, "Zbývá hod: %02d min: %02d sec: %02d");
                timeText.setText(textMillisecondUntilFinished);
            }

            /*
            při skončení odpočtu provede tyto akce: nastaví text v layoutu, vynuluje časovač (na null), nastaví toggle button, spustí přehrávání,
            uloží záznam do Db a vypíše celkový čas záznamů z Db
             */
            @Override
            public void onFinish() {
                timeText.setText("Done");
                timeRemaning = null;
                startPauseBtn.setChecked(false);
                soundPlayer.playSound(context, "tijuana_taxi_herb_alpert");

                // uložím si záznam proběhlého odčítání času do db
                serviceDbTimeMillisec.save(new LogTimeMillisec(timeForLog));

                // spočítám si celkový čas ze všech záznamů
                Long timeDb = new Long(0);
                for(LogTimeMillisec logTime: serviceDbTimeMillisec.listAll()) {
                    timeDb += logTime.getMillisec();
                }

                String textTotalTimeFromDb = timeFormatter.formatMillisecondToHourMinuteSecond(timeDb, "Čekáním jste strávili hod: %02d min: %02d sec: %02d");
                timeTotalFromDB.setText(textTotalTimeFromDb);
            }
        }.start();
    }

    // pozastavení odpočítávání (časovač se nevynuluje (není null))
    public void pauseTimer() {
        countDownTimer.cancel();
    }

    // jestliže časovače není vynulovaný, tak se zresetuje (nastaví se na null)
    public void stopTimer() {
        // může se např. stát pokud uživatel klikne na stop, pokud časovač ještě nikdy předtím nestustil
        if (countDownTimer != null) {
            this.pauseTimer();
            timeRemaning = null;
        }
    }

    /**
     * vrací časový údaj převedený na milisekundy získaný od uživatele
     * pokud uživatel zadal nesprávný vstup, vratí error, tj hodnotu -1
     *
     * @param userInput vstup získaný od uživatele
     * @param timeType  jednotky zadaného času
     * @return
     */
    public Long getTimeInMillisec(EditText userInput, TimeType timeType) {

        Editable inputUserEditable = userInput.getText();
        Long errorCode = new Long(-1);
        Long timeInMillisec;

        // kontrola uživatelského vstupu
        if (TextUtils.isEmpty(inputUserEditable)) {
            userInput.setError("Nevyplnili jste pole.");
            return errorCode;
        }
        // kontrola uživatelského vstupu
        if (!TextUtils.isDigitsOnly(inputUserEditable)) {
            userInput.setError("Nezadali jste číslo.");
            return errorCode;
        }

        String userInputStr = inputUserEditable.toString();
        int coefficient = 1;

        switch (timeType) {
            case HOUR:
                coefficient = 1000 * 60 * 60;
                break;
            case MINUTE:
                coefficient = 1000 * 60;
                break;
            case SECOND:
                coefficient = 1000;
                break;
            default:
                System.out.println("Zdali jste nepodporovaný TimeType");
                return errorCode;
        }
        timeInMillisec = Long.parseLong(userInputStr) * coefficient;
        return timeInMillisec;
    }


    public CountDownTimer getCountDownTimer() {
        return countDownTimer;
    }

    public void setCountDownTimer(CountDownTimer countDownTimer) {
        this.countDownTimer = countDownTimer;
    }

    public Long getTimeRemaning() {
        return timeRemaning;
    }

    public void setTimeRemaning(long timeRemaning) {
        this.timeRemaning = timeRemaning;
    }
}
