package com.example.timer.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.timer.R;
import com.example.timer.presenter.LogAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogListActivity extends AppCompatActivity {

        @BindView(R.id.log_list)
        ListView logList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_log);
        ButterKnife.bind(this);

        LogAdapter logAdapter = new LogAdapter(this);
        logList.setAdapter(logAdapter);




    }


}
