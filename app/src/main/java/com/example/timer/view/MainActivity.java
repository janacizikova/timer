package com.example.timer.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.timer.R;
import com.example.timer.model.LogTimeInput;
import com.example.timer.model.TimeType;
import com.example.timer.presenter.SoundPlayer;
import com.example.timer.presenter.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    // prvky layoutu
    @BindView(R.id.hours_edit)
    EditText hourEdit;
    @BindView(R.id.minutes_edit)
    EditText minuteEdit;
    @BindView(R.id.seconds_edit)
    EditText secondEdit;
    @BindView(R.id.time_txt)
    TextView timeText;
    @BindView(R.id.stop_btn)
    Button stopBtn;
    @BindView(R.id.start_pause_toggle_btn)
    ToggleButton startPauseBtn;
    @BindView(R.id.time_total_from_db_txt)
    TextView timeTotalFromDB;

    // service třídy
    private Timer timer;


    // nastevní overflow menu - získání menu layoutu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();

        // nastavení mého menu (z res)
        menuInflater.inflate(R.menu.overflow_menu, menu);

        return true;
    }

    //  nastevní overflow menu - spuštění akce při kliknutí na prvek menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //získání layoutu, kde má být overflow menu
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.activity_main_layout);

        // získání id kliknuté položky v menu
        int itemId = item.getItemId();
        if (itemId == R.id.show_log_list) {

            startActivity(new Intent(MainActivity.this, LogListActivity.class));
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        timer = new Timer();

        /*
        zastavení Timeru (časovače)
        vynuluje časovač (tzn. nastaví timeremaining Timeru na null), nastaví textView, stopne přehrávání hudby
        nastaví toggle button do pozice start (isChecked = false) (protože stop můžu použít, i když je časovač pauznutý, a aby byl časovač vynulovaný a zároveň pauznutý je nesmysl
        */
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.stopTimer();
                timeText.setText("");
                SoundPlayer.stopSound();
                startPauseBtn.setChecked(false);
            }
        });

        /*
        spuštění a pauznutí Timeru (časovače)
        toggle button - ve výchozím stavu je isChecked false (tzn. na button se dosud nekliklo)
         */
        startPauseBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Long timeRemaining;

                if (isChecked) {
                    timeRemaining = timer.getTimeRemaning();

                    // jestliže je časovač null, tak uživatel nikdy nespustil časovač (nestiskl start), nebo předchozí spuštěný časovač dokončil odpočítávání
                    // a chci tedy získat hodnotu času z uživatelského vstupu
                    if (timeRemaining == null) {


                        // je možné, že se přehrává hudba z předchozího dokončeného časovače - proto při novém spuštění časovače, hudbu zastav
                        SoundPlayer.stopSound();

                        // získání milisekund z uživatelem zadaných hodnot (sekund, minut, hodin)
                        Long milisecondSecond = timer.getTimeInMillisec(secondEdit, TimeType.SECOND);
                        Long milisecondMinute = timer.getTimeInMillisec(minuteEdit, TimeType.MINUTE);
                        Long milisecondHour = timer.getTimeInMillisec(hourEdit, TimeType.HOUR);

                        // -1 značí nesprávný uživatelský vstup a použije se pro porovnání, zda uživatel zadal korektní vstup
                        Long checkLong = new Long(-1);

                        if (milisecondHour.equals(checkLong) || milisecondMinute.equals(checkLong) || milisecondSecond.equals(checkLong)) {
                            // jestliže je nesprávný uživatelský vstup, nastavíme toggle button jako nestisknutý, aby bylo možné zkusit spustit nový start
                            startPauseBtn.setChecked(false);
                            return;
                        }
                        timeRemaining = milisecondHour + milisecondMinute + milisecondSecond;
                    }
                    timer.startTimer(timeRemaining, timeText, timeTotalFromDB, startPauseBtn, MainActivity.this);
                } else {
                    timer.pauseTimer();
                }
            }
        });
    }

    // je možné, že se ještě přehrává hudba z předchozího dokončeného časovače, proto při shození aplikace do pozadí, hudbu zastav
    @Override
    protected void onStop() {
        super.onStop();
        SoundPlayer.stopSound();
    }
}
